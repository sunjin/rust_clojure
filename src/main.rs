use std::thread;
use std::time::Duration;

fn main() {
    println!("Hello, world!");

    let simurated_user_specified_value = 10;
    let simulated_random_number = 7;

    generate_workout(simurated_user_specified_value, simulated_random_number);
}

struct Cacher<T>
where
    T: Fn(u32) -> u32,
{
    calculation: T,
    value: Option<u32>,
}

impl<T> Cacher<T>
where
    T: Fn(u32) -> u32,
{
    fn new(calcutation: T) -> Cacher<T> {
        Cacher {
            calculation: calcutation,
            value: None,
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            }
        }
    }
}

fn generate_workout(intensity: u32, random_number: u32) {
    let mut expensive_result = Cacher::new(|num| {
        println!("計算しています...");
        thread::sleep(Duration::from_secs(2));
        num
    });

    if intensity < 25 {
        println!(
            "今日は{}回腕立て伏せをしてください",
            expensive_result.value(intensity)
        );

        println!(
            "次に、{}回腹筋をしてください",
            expensive_result.value(intensity)
        );

        return;
    }

    if random_number == 3 {
        println!("今日は休憩");
        return;
    }

    println!(
        "今日は、{}分間走ってください",
        expensive_result.value(intensity)
    );
}
